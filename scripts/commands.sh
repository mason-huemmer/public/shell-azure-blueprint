#!/usr/bin/env bash


### Environment Variables
##############################################################################

# Base
VERSION="${VERSION:-}"

# Azure Login
AZURE_USERNAME="${AZURE_USERNAME:-}"
AZURE_PASSWORD="${AZURE_PASSWORD:-}"
AZURE_SUBSCRIPTION="${AZURE_SUBSCRIPTION:-}"
AZURE_TENANT="${AZURE_TENANT:-}"

# Azure Cli
AZURE_CLI_OUTPUT="${AZURE_CLI_OUTPUT:-}"

# Azure Blueprint
AZURE_BLUEPRINT_NAME="${AZURE_BLUEPRINT_NAME:-}"
AZURE_BLUEPRINT_LOCATION="${AZURE_BLUEPRINT_LOCATION:-}"
AZURE_BLUEPRINT_DIRECTORY="${AZURE_BLUEPRINT_DIRECTORY:-}"
AZURE_BLUEPRINT_DESCRIPTION="${AZURE_BLUEPRINT_DESCRIPTION:-}"
AZURE_BLUEPRINT_PARAMETERS="${AZURE_BLUEPRINT_PARAMETERS:-}"
AZURE_BLUEPRINT_VERSION="${AZURE_BLUEPRINT_VERSION:-}"
TAGGING_STRATEGY_BLUEPRINT_PARAMETERS="${TAGGING_STRATEGY_BLUEPRINT_PARAMETERS:-}"

### Public Functions
#########################################################

function load_dotenv(){

    _load_dotenv

}

function login_azure(){

    local exit_code=0

    _verify_cli_exists "az"

    if _login_azure; then
        : # do nothing
    else
        exit_code=${?}
        exit ${exit_code}
    fi

    return ${exit_code}

}

function import_blueprint(){

    local exit_code=0

    if _import_blueprint; then
        : # do nothing
    else
        _logout_azure
        exit_code=${?}
        exit ${exit_code}
    fi

    return ${exit_code}

}

function publish_blueprint(){

    local exit_code=0

    if _publish_blueprint; then
        : # do nothing
    else
        _logout_azure
        exit_code=${?}
        exit ${exit_code}
    fi

    return ${exit_code}

}

function assign_blueprint(){

    local exit_code=0

    if _assign_blueprint; then
        : # do nothing
    else
        _logout_azure
        exit_code=${?}
        exit ${exit_code}
    fi

    return ${exit_code}

}

### Internal-Use Only
#########################################################

function _run(){   
    echo -e "\e[92;1m$" "$*\e[0m"; "$@"
}

function _load_dotenv(){

    if [ -f .env ]; then
        set -a; 
        source .env; 
        set +a
    fi

}

function _verify_cli_exists(){

    local exit_code=0

    if ! command -v ${1} &> /dev/null; \
    then
        echo -e "\e[91;1mERROR: Can not find '${1}' executable in PATH\e[0m" 
        exit_code=1  
    fi

    return ${exit_code}
}

function _login_azure(){

    local exit_code=0

    if eval "_run az login --service-principal \
        --username ${AZURE_USERNAME} \
        --password ${AZURE_PASSWORD} \
        --tenant ${AZURE_TENANT}"; \
    then
        _configure_azure_cli
    else
        exit_code=${?}
    fi

    return ${exit_code}

}

function _configure_azure_cli(){

    local exit_code=0

    if eval "_run az config set extension.use_dynamic_install=yes_without_prompt \
        --output ${AZURE_CLI_OUTPUT} \
        --only-show-errors"; \
    then
        : # do nothing
    else
        exit_code=${?}
    fi

    return ${exit_code}

}

function _import_blueprint(){

    local exit_code=0

    if eval "_run az blueprint import --subscription ${AZURE_SUBSCRIPTION} \
        --name ${AZURE_BLUEPRINT_NAME} \
        --input-path '${AZURE_BLUEPRINT_DIRECTORY}' \
        --output ${AZURE_CLI_OUTPUT} \
        --only-show-errors \
        --yes"; \
    then
        : # do nothing
    else
        exit_code=${?}
    fi

    return ${exit_code}
}

function _publish_blueprint(){

    local exit_code=0

    if eval "_run az blueprint publish --subscription ${AZURE_SUBSCRIPTION} \
        --blueprint-name ${AZURE_BLUEPRINT_NAME} \
        --version ${VERSION} \
        --output ${AZURE_CLI_OUTPUT} \
        --only-show-errors"; \
    then
        : # do nothing
    else
        exit_code=${?}
    fi

    return ${exit_code}

}

function _assign_blueprint(){

    local exit_code=0

    if _show_assignment; then
        if _update_assignment; then
            : # do nothing
        else
            exit_code=${?}
        fi
    else
        if _create_assignment; then
            : # do nothing
        else
            exit_code=${?}
        fi
    fi

    return ${exit_code}

}

function _show_assignment(){

    local exit_code=0
    
    # redirect stderr to /dev/null
    if eval "_run az blueprint assignment show --subscription ${AZURE_SUBSCRIPTION} \
        --name ${AZURE_BLUEPRINT_ASSIGNMENT} \
        --output ${AZURE_CLI_OUTPUT} \
        --only-show-errors";
    then
        : # do nothing
    else
        exit_code=${?}
    fi

    return ${exit_code}

}

function _create_assignment(){

    local exit_code=0

    if eval "_run az blueprint assignment create --subscription ${AZURE_SUBSCRIPTION} \
        --name ${AZURE_BLUEPRINT_ASSIGNMENT} \
        --location ${AZURE_BLUEPRINT_LOCATION} \
        --identity-type SystemAssigned \
        --locks-mode None \
        --blueprint-version ${AZURE_BLUEPRINT_VERSION} \
        --parameters ${TAGGING_STRATEGY_BLUEPRINT_PARAMETERS} \
        --description '${AZURE_BLUEPRINT_DESCRIPTION}' \
        --output ${AZURE_CLI_OUTPUT} \
        --only-show-errors"; 
    then
        _check_assignment_event "created"
    else
        exit_code=${?}
    fi

    return ${exit_code}

}

function _update_assignment(){

    local exit_code=0
    
    if eval "_run az blueprint assignment update --subscription ${AZURE_SUBSCRIPTION} \
        --name ${AZURE_BLUEPRINT_ASSIGNMENT} \
        --blueprint-version ${AZURE_BLUEPRINT_VERSION} \
        --parameters ${TAGGING_STRATEGY_BLUEPRINT_PARAMETERS} \
        --output ${AZURE_CLI_OUTPUT} \
        --only-show-errors"; 
    then
        _check_assignment_event "updated"
    else
        exit_code=${?}
    fi

    return ${exit_code}

}

function _check_assignment_event(){

    local exit_code=0 event_arg=""

    # Add Event Argument
    case ${1} in 
        "created")
            echo "test"
            event_arg="--created"
            ;;
        "updated")
            event_arg="--updated"
            ;;
    esac

    if eval "_run az blueprint assignment wait --subscription ${AZURE_SUBSCRIPTION} \
        --name ${AZURE_BLUEPRINT_ASSIGNMENT} \
        --output ${AZURE_CLI_OUTPUT} \
        '${event_arg}'"; \
    then
        : # do nothing
    else
        exit_code=${?}
    fi

    return ${exit_code}

}

function _logout_azure(){

    local exit_code=0

    if _run az logout; \
    then
        : # do nothing
    else
        exit_code=${?}
    fi

    return ${exit_code}

}